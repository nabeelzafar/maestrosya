<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function attachments()
    {
    	return $this->hasMany(OrderAttachment::class, 'order_id', 'id');
    }

    public function service()
    {
    	return $this->belongsTo(Service::class, 'service_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function employee()
    {
    	return $this->belongsTo(User::class, 'employee_id', 'id');
    }
}
