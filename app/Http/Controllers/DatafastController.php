<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;

class DatafastController extends Controller
{

//     ID de entidad: 8ac7a4c884e711930184ee587f9a2127
// ACCESS TOKEN: OGE4Mjk0MTg1YTY1YmY1ZTAxNWE2YzhjNzI4YzBkOTV8YmZxR3F3UTMyWA==
// MID:1000000505 
// TID: PD100406 
// SERV: 17913101 
    //
    protected $token = "OGE4Mjk0MTg1YTY1YmY1ZTAxNWE2YzhjNzI4YzBkOTV8YmZxR3F3UTMyWA==";
    // protected $token = "OGE4MjkOMTg1YTY1YmY1ZTA×NWE2YzhjNzI4YzBkOTV8YmZxR3F3UTMyWA==";
    protected $production = false;// this should be set to true in production
    // protected $entityId = "8ac7a4c884e711930184ee587f9a2127";//
    // protected $entityId = "8a8294175f113aad015f11652f2200a5";//
    protected $entityId = "8ac7a4ca8611ddb1018612ac34eb0243";//
    

    public function GenerateCheckouId(Request $request,$order_id) 
    {
        $order = Order::where('id',$order_id)->where('payment_status','pending')->first();
        if($order){

            $userInfo = User::find($order->user_id);
            //dd($userInfo);

            //Message issued when taxes have been miscalculated, base 0% + base 12% + VAT value must be equal to the amount being sent.
            $amount = $order->cost;
            $SHOPPER_VAL_IVA = 0.12;//($amount/100)*12;
            $SHOPPER_VAL_BASEIMP = 1.00;//$amount - $SHOPPER_VAL_IVA;
            $grandSum = $amount + $SHOPPER_VAL_IVA + $SHOPPER_VAL_BASEIMP;
            $SHOPPER_VAL_BASE0 = number_format((float)$order->cost, 2, '.', '');
            //dd($amount.'------'.$SHOPPER_VAL_IVA.'-----'.$SHOPPER_VAL_BASEIMP);
            
            $url = "https://eu-test.oppwa.com/v1/checkouts";
            $data = "entityId=".$this->entityId.
            "&amount=".number_format((float)$grandSum, 2, '.', '').
            "&currency=USD".
            "&paymentType=DB".
            "&customParameters[SHOPPER_MID]=1000000505".
            "&customParameters[SHOPPER_TID]=PD100406".
            //"&customParameters[SHOPPER_ECI]=0103910".
            "&customParameters[SHOPPER_PSERV]=17913101".
            "&risk.parameters[USER_DATA2]=MAESTROYA".
            "&customParameters[SHOPPER_VAL_BASE0]=".$SHOPPER_VAL_BASE0.
            "&customParameters[SHOPPER_VAL_BASEIMP]=".$SHOPPER_VAL_BASEIMP.
            "&customParameters[SHOPPER_VAL_IVA]=".$SHOPPER_VAL_IVA.
            "&customParameters[SHOPPER_VERSIONDF]=2".
            "&merchantTransactionId=01082023_061055_".$order_id.
            "&customer.givenName=".$userInfo->name.
            "&customer.middleName=".$userInfo->name.
            "&customer.surname=".$userInfo->name.
            "&customer.email=" .$userInfo->email.
            "&customer.phone=".$userInfo->mobile_no.
            "&customer.ip=".$request->ip().
            "&billing.street1=HafizJamal,Multan&billing.country=PK&billing.postcode=60000".
            "&testMode=EXTERNAL";
            //customer.merchantCustomerID
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:Bearer '.$this->token));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->production);// this should be set to true in production
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $responseData = curl_exec($ch);
            if(curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);
            $result =  json_decode($responseData);
            //dd($result);
            $order->checkoutid = $result->id;
            $order->save();
            return response()->json($result);
            
        } else {
            return response()->json(['error' => 'No Order found' ]);
        }
        
     }

     public function PaymentStatus($order_id) {

        $order = Order::where('id',$order_id)->whereNull('checkoutid')->first();
        if($order){

            $checkoutid = $order->checkoutid;
            $md5_id = md5($order_id);
            
            $url = "https://eu-test.oppwa.com/v1/checkouts/".$checkoutid."/payment";
            $url .= "?entityId=".$this->entityId;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:Bearer '.$this->token));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $responseData = curl_exec($ch);
            if(curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);
            //return $responseData;
            return response()->json($responseData);


        } else {
            return response()->json(['error' => 'No Order found' ]);
        }

     }
}
