<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderAttachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    //
    public function index(Request $request)
    {
        try {

            $user = \Auth::user();
            $orders = Order::with('attachments','service')->where('user_id',$user->id)->get();
            return response()->json($orders);

        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }

    }


    public function save(Request $request){

        try{

            $user = \Auth::user();
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'service_id' => 'required|exists:services,id',
            ]);

            if ($validator->fails()) {
                return response()->json(['error', $validator->errors()]);
            }

            $order = new Order();
            $order->title = $request->title;
            $order->service_id = $request->service_id;
            $order->description = ($request->description) ? $request->description:'';
            $order->user_id  = $user->id;
            $order->save();

            if ($request->hasFile('files')) {

                foreach($request->file('files') as $file){

                    $attachment = new OrderAttachment();
                    $extention = $file->getClientOriginalExtension();
                    $filename = time() . '.' . $extention;
                    $file->move(public_path().'/assets/attachments/', $filename);
                    $attachment->attachment = '/assets/attachments/'.$filename;
                    $attachment->order_id = $order->id;
                    $attachment->save();
                }
            }


            return response()->json($order);

        } catch(\Exception $e){
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }

    }


    public function updateStatus(Request $request){

        try{

            $user = \Auth::user();
            $validator = Validator::make($request->all(), [
                'order_id' => 'required|exists:orders,id',
                'status' => 'in:in progress,completed,canceled',
            ]);

            if ($validator->fails()) {
                return response()->json(['error', $validator->errors()]);
            }

            $order = Order::where('id',$request->order_id)->where('user_id',$user->id)->first();
            $order->status  = $request->status;
            $order->save();
            return response()->json($order);

        } catch(\Exception $e){
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }

    }

    public function detail(Request $request,$order_id){

        try{

            $user = \Auth::user();
            $order = Order::with('attachments','service')->where('id',$order_id)->where('user_id',$user->id)->first();
            if($order){
                return response()->json($order);
            } else {
                return response()->json(['error' => 'Not Record found. ']);
            }

        } catch(\Exception $e){
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }

    }

    public function orderByStatus(Request $request,$status){

        try{

            //'pending','in progress','completed','canceled'

            $user = \Auth::user();
            $order = Order::with('attachments','service')->where('status',$status)->where('user_id',$user->id)->get();
            if($order){
                return response()->json($order);
            } else {
                return response()->json(['error' => 'Not Record found. ']);
            }

        } catch(\Exception $e){
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }

    }

    public function orderByCategory(Request $request,$service_id){

        try{

            //'pending','in progress','completed','canceled'

            $user = \Auth::user();
            $order = Order::with('attachments','service')->where('service_id',$service_id)->where('user_id',$user->id)->get();
            if($order){
                return response()->json($order);
            } else {
                return response()->json(['error' => 'Not Record found. ']);
            }

        } catch(\Exception $e){
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }

    }
}
