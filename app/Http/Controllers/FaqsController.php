<?php

namespace App\Http\Controllers;

use App\Faqs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faqs::all();
        $_view_data = [];

        foreach ($faqs as $key => $value) {
            $edit = '<a href="/faq/edit/' . $value->id . '" class="text-decoration-none"><i class="fa fa-file-lines"></i> Edit</a>';
            $delete = '<a href="/faq/delete/' . $value->id . '" class="text-decoration-none"><i class="fa fa-trash-can"></i> Delete</a>';
            $action =  $edit . ' | ' . $delete;

            $_view_data[] = [
                "title" =>  $value->title,
                "description" =>  $value->description,
                "status" =>  $value->status,
                "action" => $action,
            ];
        }

        $data = [
            'page_title' => "FAQ's Listing",
            '_view_data' => $_view_data,
        ];
        return view('pages.FAQs.faqs_listing', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'page_title' => "Add FAQ",
            '_url' => '/faq/store',
        ];
        return view('pages.FAQs.faq_add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string'],
            'description' => ['required', 'string'],
            'status' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        try {
            $faq = new Faqs();
            $faq->title = $request->title;
            $faq->description = $request->description;
            $faq->status = $request->status;
            $faq->save();

            return redirect('/faq')->with('success', 'FAQs Add Successfully');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    // Employee Delete 
    public function delete($id)
    {
        $faq = Faqs::findOrFail($id);

        if ($faq) {
            $faq->delete();
            return redirect('/faq')->with('success', 'FAQs Delete Successfully');
        } else {
            return redirect()->back()->with('error', 'FAQs could not be Deleted.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faqs  $faqs
     * @return \Illuminate\Http\Response
     */
    public function show(Faqs $faqs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faqs  $faqs
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faqs::findOrFail($id);
        $data = [
            'page_title' => "Add FAQ",
            '_view_data' => $faq,
            '_url' => '/faq/update/'.$id,
        ];
        return view('pages.FAQs.faq_add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faqs  $faqs
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string'],
            'description' => ['required', 'string'],
            'status' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        try {
            $faq = Faqs::findOrFail($id);
            $faq->title = $request->title;
            $faq->description = $request->description;
            $faq->status = $request->status;
            $faq->save();

            return redirect('/faq')->with('success', 'FAQs Update Successfully');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faqs  $faqs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faqs $faqs)
    {
        //
    }
}
