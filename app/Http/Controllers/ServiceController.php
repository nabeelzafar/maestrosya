<?php

namespace App\Http\Controllers;

use App\Faqs;
use App\Order;
use App\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log; 

class ServiceController extends Controller
{
    //

    public function checkout(Request $request,$order_id)
    {

        try {

            $response = new DatafastController();
            $checkout_id = $response->GenerateCheckouId($request,$order_id);
            $content = $checkout_id->getContent();
            $array = json_decode($content, true);
            $checkout_content = $array['id'];
            $data = [
                'checkout_content' => $checkout_content,
            ];
            return view('checkout',$data);


        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage().'--'.$e->getFile().'--'.$e->getLine()]);
        }
    }

    public function sandboxStatus(Request $request){


        //8ac7a4ca8611ddb1018612ac34eb0243
        //8ac7a4c884e711930184ee587f9a2127
        $url = "https://eu-test.oppwa.com/v1/checkouts/".$request->id."/payment";
        $url .= "?entityId=8ac7a4ca8611ddb1018612ac34eb0243";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:Bearer OGE4Mjk0MTg1YTY1YmY1ZTAxNWE2YzhjNzI4YzBkOTV8YmZxR3F3UTMyWA=='));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        $array = json_decode($responseData, true);
        Log::info($array);
        if($array['result']['code'] == '000.100.112'){
            $order = Order::where('checkoutid',$request->id)->first();
            $order->status = 'in progress';
            $order->payment_status = 'approve';
            $order->payment_at = Carbon::now()->format('Y-m-d');
            $order->save();
            //echo 'payment completed thanks';
            return redirect()->to('http://maestrosya.ec/order/payment/success');

        }else{
            $messsage = 'payment not completed please try again later';
            $tile = 'error';
            return redirect()->to("http://maestrosya.ec/order/payment/error?title=".$tile."&message=".$messsage);
        }
        
        //dd($array['result']['code']);

    }

    public function index(Request $request)
    {

        try {

            $data = Service::all();
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }
    }

    public function getFAQ(Request $request)
    {
        try {

            $data = Faqs::where('status','active')->get();
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }
        
    }

    public function serviceListing()
    {
        $services = Service::all();
        $_view_data = [];
        foreach ($services as $key => $value) {
            $_view_data[] = [
                'id' => $value->id,
                'name' => $value->name,
                'details' => $value->details,
            ];
        }

        $data = [
            'page_title' => "Service Listing",
            '_view_data' => $_view_data,
        ];
        return view('pages.services.service_listing', $data);
    }

    public function preparation(Request $request)
    {

        try {

            // $response = new DatafastController();
            // $checkout_id = $response->GenerateCheckouId($order_id);
            // $content = $checkout_id->getContent();
            // $array = json_decode($content, true);
            // $checkout_content = $array['id'];
            // $data = [
            //     'checkout_content' => $checkout_content,
            // ];
            return view('preparation');


        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage().'----'.$e->getLine()]);
        }
    }
}
