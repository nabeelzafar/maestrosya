<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Twilio\Http\GuzzleClient;
use Twilio\Rest\Client;

class UserController extends Controller
{
    //
    public function register(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'mobile_no' => ['required', 'string', 'max:12'],
            ]);

            if ($validator->fails()) {
                return response()->json(['error', $validator->errors()]);
            }

            $token = rand(1111, 9999);

            $finder = User::where('mobile_no', $request->mobile_no)->first();
            if ($finder) {

                $finder->password = Hash::make($token);
                $finder->save();
                $user = User::where('mobile_no', $request->mobile_no)->first();
            } else {
                $user =  User::create([
                    'name' => '',
                    'mobile_no' => $request->mobile_no,
                    'email' => $request->mobile_no . '@gmail.com',
                    'password' => Hash::make($token)
                ]);
            }

            $data = array(
                'user' => $user,
                'otp' => $token
            );

            //$this->sendWhatsappNotification($token,$user->mobile_no);

            return response()->json([$data, 'User register successfully.']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }
    }

    public function login(Request $request)
    {
        try {

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

                $user = \Auth::user();
                $token = $user->createToken('Services');
                $user['token'] = $token->accessToken;
                return response()->json([$user, 'User login successfully.']);
            } else {
                return response()->json(['error' => 'No Record Found']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }
    }

    public function verify(Request $request)
    {

        try {

            if (Auth::attempt(['mobile_no' => $request->mobile_no, 'password' => $request->otp])) {

                $user = \Auth::user();
                $token = $user->createToken('Services');
                $success['token'] = $token->accessToken;
                return response()->json([$success, 'User login successfully.']);
            } else {
                return response()->json(['Wrong Username or Password.', ['error' => 'Unauthorised']]);
            }
        } catch (\Exception $e) {
            return response()->json(['Please try again.', ['error' => 'Unauthorised ' . $e->getMessage()]]);
        }
    }

    public function profile(Request $request)
    {
        try {

            $user = \Auth::user();
            return response()->json($user);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }
    }

    public function updateProfile(Request $request)
    {
        $user = \Auth::user();

        if ($request->has('name')) {
            $user->name = $request->name;
        }

        if ($request->has('email')) {
            $user->email = $request->email;
        }

        if ($request->has('mobile_no')) {
            $user->mobile_no = $request->mobile_no;
        }

        if ($request->has('address')) {
            $user->address = $request->address;
        }

        if ($request->hasFile('avatar')) {
            $image = $request->file('avatar');
            $avatar = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/users');
            $image->move($destinationPath, $avatar);
            $user->avatar = $avatar;
        }

        $user->save();
        //$success = $this->getUserInfoObject($user);
        //return $this->sendResponse($success, 'User Profile Updated successfully.');
        return response()->json(\Auth::user());
    }

    public function userListing()
    {
        $users = User::where('user_type', 'user')->get();
        $_view_data = [];
        foreach ($users as $key => $value) {
            $_view_data[] = [
                "name" =>  $value->name,
                // "email" =>  $value->email,
                "mobile_no" =>  $value->mobile_no,
                "address" =>  $value->address,
            ];
        }

        $data = [
            'page_title' => "User Listing",
            '_view_data' => $_view_data,
        ];
        return view('pages.users.user_listing', $data);
    }

    private function sendWhatsappNotification(string $otp, string $recipient)
    {
        $token = 'sendchamp_live_$2a$10$eWyl9YKCjj6bZXSTWAtMiOr/sD/EEldQnJIHhwRJFNGFwp.mTJOi6';
        $client = new \GuzzleHttp\Client();
        $code = 'Your OTP number is '.$otp;
        $response = $client->request('POST', 'https://api.sendchamp.com/api/v1/whatsapp/message/send', [
            'body' => '{"message":"'.$code.'","type":"text","sender":"923337648338","recipient":"'.$recipient.'"}',
            'headers' => [
                'Authorization' => 'Bearer '.$token,
                'accept' => 'application/json',
                'content-type' => 'application/json',
            ],
        ]);

        return $response->getBody();
    }

    // Employee Listing 
    public function employeeListing()
    {
        $users = User::where('user_type', 'employee')->get();
        $_view_data = [];

        foreach ($users as $key => $value) {
            $edit = '<a href="/employee/edit/'. $value->id .'" class="text-decoration-none"><i class="fa fa-file-lines"></i> Edit</a>';
            $delete = '<a href="/employee/delete/'. $value->id .'" class="text-decoration-none"><i class="fa fa-trash-can"></i> Delete</a>';
            $action = $edit .' | '. $delete;

            $_view_data[] = [
                "name" =>  $value->name,
                "email" =>  $value->email,
                "mobile_no" =>  $value->mobile_no,
                "address" =>  $value->address,
                "action" => $action,
            ];
        }

        $data = [
            'page_title' => "Employee Listing",
            '_view_data' => $_view_data,
        ];
        return view('pages.employee.employee_listing', $data);
    }

    // Employee Add 
    public function employeeAdd()
    {
        $data = [
            'page_title' => "Add Employee",
            '_url' => '/employee/store',
        ];
        return view('pages.employee.employee_add', $data);
    }

    // Employee Delete 
    public function employeeDelete($id)
    {
        $user = User::findOrFail($id);

        if ($user) {
            $user->delete();
            return redirect('/employee')->with('success', 'Employee Delete Successfully');
        } else {
            return redirect()->back()->with('error', 'Employee could not be Deleted.');
        }
    }

    // Employee Store 
    public function employeeStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users,email'],
            'user_type' => ['required', 'string'],
            'address' => ['required', 'string'],
            'mobile_no' => ['required', 'string', 'max:12','unique:users,mobile_no'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->user_type = $request->user_type;
            $user->address = $request->address;
            $user->mobile_no = $request->mobile_no;
            $user->password = Hash::make($request->email);
            $user->save();

            return redirect('/employee')->with('success', 'Employee Add Successfully');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    // Employee Edit 
    public function employeeEdit($id)
    {
        $user = User::findOrFail($id);
        $data = [
            'page_title' => "Edit Employee",
            '_view_data' => $user,
            '_url' => '/employee/update/'.$id,
        ];
        return view('pages.employee.employee_add', $data);
    }

    // Employee Update 
    public function employeeUpdate($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'user_type' => ['required', 'string'],
            'address' => ['required', 'string'],
            'mobile_no' => ['required', 'string', 'max:12'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        try {
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->address = $request->address;
            $user->mobile_no = $request->mobile_no;
            $user->save();

            return redirect('/employee')->with('success', 'Employee Update Successfully');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
