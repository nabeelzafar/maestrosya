<?php

namespace App\Http\Controllers;

use App\Order;
use App\Service;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdminOrderController extends Controller
{
    //
    public function index(Request $request)
    {
        try {

            $user = Auth::user();
            $orders = Order::with('attachments', 'service')->get();
            return response()->json($orders);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }
    }


    public function updateOrder(Request $request)
    {

        try {

            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'order_id' => 'required|exists:orders,id',
                'status' => 'in:in progress,completed,canceled',
            ]);

            if ($validator->fails()) {
                return response()->json(['error', $validator->errors()]);
            }

            $order = Order::where('id', $request->order_id)->first();
            $order->status  = $request->status;
            $order->notes  = ($request->notes) ? $request->notes : "";
            $order->cost  = ($request->cost) ? $request->cost : 0;
            $order->save();

            return response()->json($order);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Unauthorised ' . $e->getMessage()]);
        }
    }

    // ******************************* UI For Dashboard *****************************************
    public function dashboard()
    {
        $new_users_data = User::where('user_type', 'user')->orderBy('created_at', 'desc')->take(10)->get();
        $new_users = [];
        $pending_orders_data = Order::where('status', 'pending')->orderBy('created_at', 'desc')->take(10)->get();
        $pending_orders = [];
        $paid_orders_data = Order::where('payment_status', 'approve')->orderBy('created_at', 'desc')->take(10)->get();
        $paid_orders = [];

        foreach ($new_users_data as $key => $value) {
            $new_users[] = [
                "name" =>  $value->name,
                "email" =>  $value->email,
                "mobile_no" =>  $value->mobile_no,
                "address" =>  $value->address,
            ];
        }

        foreach ($pending_orders_data as $key => $value) {
            $pending_orders[] = [
                "title" => $value->title,
                "service_name" => $value->service->name,
                "description" => $value->description,
                "status" => $value->status,
            ];
        }

        foreach ($paid_orders_data as $key => $value) {
            $paid_orders[] = [
                "title" => $value->title,
                "service_name" => $value->service->name,
                "description" => $value->description,
                "payment_status" => $value->payment_status,
            ];
        }

        $data = [
            'page_title' => "Dashboard",
            'total_users' => User::where('user_type', 'user')->count(),
            'total_orders' => Order::count(),
            'total_employees' => User::where('user_type', 'employee')->count(),
            'total_sale' => Order::where('payment_status','approve')->sum('cost'),
            '_new_users' => $new_users,
            '_pending_orders' => $pending_orders,
            '_paid_orders' => $paid_orders,
        ];

        return view('pages.dashboard.dashboard', $data);
    }

    // ******************************* UI For Calendar *****************************************
    public function calendarView()
    {
        $orders = Order::with('employee')->get();
        $order_array = [];
        foreach ($orders as $key => $value) {
            if (isset($value->employee) && isset($value->schedule_at)) {
                array_push($order_array, [
                    'title' => $value->title . ' - ' . $value->employee->name,
                    'start' => preg_replace('/\s+/', 'T', $value->schedule_at)
                ]);
            }
        }

        $data = [
            'page_title' => "Calendar",
            '_view_data' => $order_array,
        ];
        return view('pages.calendar.calendar', $data);
    }

    // ******************************* UI For Order *****************************************

    public function orderListing()
    {
        $orders = Order::with('user','attachments', 'service')->get();
        //dd($orders);

        $_view_data = [];
        foreach ($orders as $key => $value) {
            //dd($value);
            $_edit = '<a href="/order/edit/' . $value->id . '"><i class="far fa-edit"></i> Edit</a>';
            $_view_data[] = [
                "title" => $value->title,
                "service_name" => $value->service->name,
                "customer" => array( 'name' => $value->user->name,"mobile_no" => $value->user->mobile_no,),
                "description" => $value->description,
                "notes" => $value->notes,
                "cost" => $value->cost,
                "payment_method" => $value->payment_method,
                "status" => $value->status,
                "action" => $_edit,
            ];
        }

        $data = [
            'page_title' => "Order Listing",
            '_view_data' => $_view_data,
            'today_sale' => Order::where('payment_status','approve')->whereDate('payment_at',Carbon::today())->sum('cost'),
            'weekly_sale' => Order::where('payment_status','approve')->whereDate('payment_at','<=',Carbon::today())->whereDate('payment_at','>=',Carbon::now()->subDays(7))->sum('cost'),
            'total_sale' => Order::where('payment_status','approve')->sum('cost')
        ];
        return view('pages.orders.order_listing', $data);
    }

    public function orderEdit($id)
    {
        $order = Order::with('user','attachments', 'service')->find($id)->toArray();
        $employee = User::where('user_type', 'employee')->get()->toArray();
        $data = [
            'page_title' => "Edit Order",
            '_view_data' => $order,
            '_employee' => $employee,
        ];
        return view('pages.orders.edit_order', $data);
    }

    public function orderUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'employee_id' => 'required|exists:users,id',
            'status' => 'in:in progress,completed,canceled,awaiting_payment',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors());
        }

        $order = Order::where('id', $request->order_id)->first();
        $order->status  = $request->status;
        $order->employee_id  = $request->employee_id;
        $order->schedule_at  = $request->schedule_at;
        $order->notes  = ($request->notes) ? $request->notes : "";
        $order->cost  = ($request->cost) ? $request->cost : 0;
        $order->save();

        return redirect('/home')->with('success', "Order Update Successfully ");
        // $order = Order::with('attachments', 'service')->find($id)->toArray();
        // $data = [
        //     'page_title' => "Edit Order",
        //     '_view_data' => $order,
        // ];
        // return view('pages.orders.edit_order', $data);
    }
}
