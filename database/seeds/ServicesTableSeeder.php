<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //DB::table('services')->truncate();
        $services = [
            [
                'name' => 'Instalacion',
                'details' => 'Nuevos productos'
            ],
            [
                'name' => 'Plomeria',
                'details' => 'Sifones, duchas y mas
                '
            ],
            [
                'name' => 'Pintar',
                'details' => 'Pared, techo y mas'
            ],
            [
                'name' => 'Construccion',
                'details' => 'Civil y mas'
            ],
            [
                'name' => 'Otros',
                'details' => 'Tiene algo diferente o varias cosas que arregla e instalar'
            ]
        ];
        DB::table('services')->insert($services);
    }
}
