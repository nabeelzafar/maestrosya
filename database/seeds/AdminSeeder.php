<?php

use App\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        if (User::where('email', 'admin@admin.com')->first() === NULL) {

            $input['name'] = 'admin';
            $input['email'] = 'admin@admin.com';
            $input['mobile_no'] = '9999999999';
            $input['password'] = \Illuminate\Support\Facades\Hash::make('admin@123');
            $input['user_type'] = User::$USER_TYPE_ADMIN;
            User::create($input);
        }
    }
}
