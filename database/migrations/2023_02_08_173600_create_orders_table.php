<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->unsignedBigInteger('service_id');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->text('description')->nullable();
            $table->text('notes')->nullable();
            $table->integer('cost')->nullable();
            $table->enum('payment_method',['Cash','Credit Card']);
            $table->enum('status',['pending','in progress','completed','canceled', 'awaiting_payment']);
            $table->unsignedBigInteger('user_id');
            $table->text('checkoutid')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->dateTime('schedule_at')->nullable();
            $table->enum('payment_status',['pending','approve','cancel'])->default('pending');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
