<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('verify', 'UserController@verify');
Route::get('services', 'ServiceController@index');
Route::get('faq', 'ServiceController@getFAQ');
//Route::post('login', 'UserController@login');

Route::middleware('auth:api')->group(function () {

    //
    Route::get('profile', 'UserController@profile');
    Route::post('profile', 'UserController@updateProfile');


    Route::get('order/list', 'OrderController@index');
    Route::post('order/save', 'OrderController@save');
    Route::post('order/update/status', 'OrderController@updateStatus');
    Route::get('order/detail/{order_id}', 'OrderController@detail');
    Route::get('order/status/{status}', 'OrderController@orderByStatus');
    Route::get('order/service/{service_id}', 'OrderController@orderByCategory');
    Route::post('order/checkoutid/{order_id}', 'DatafastController@GenerateCheckouId');
    Route::post('order/payment_status/{order_id}', 'DatafastController@PaymentStatus');

    Route::get('admin/order/list', 'AdminOrderController@index');
    Route::post('admin/order/update', 'AdminOrderController@updateOrder');

});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
