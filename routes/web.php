<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/checkout/{order_id}', 'ServiceController@checkout');
Route::get('/sandboxStatus', 'ServiceController@sandboxStatus');
Route::get('/preparation', 'ServiceController@preparation');

Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', 'AdminOrderController@orderListing');
Route::get('order/edit/{id}', 'AdminOrderController@orderEdit');
Route::post('order/update', 'AdminOrderController@orderUpdate');

Route::get('/services', 'ServiceController@serviceListing');
Route::get('/users', 'UserController@userListing');





Route::prefix('/dashboard')->group(function () {
    Route::get('/', 'AdminOrderController@dashboard');
});

Route::prefix('/calendar')->group(function () {
    Route::get('/', 'AdminOrderController@calendarView');
});

Route::prefix('/employee')->group(function () {
    Route::get('/', 'UserController@employeeListing');
    Route::get('/add', 'UserController@employeeAdd');
    Route::post('/store', 'UserController@employeeStore');
    Route::get('/edit/{id}', 'UserController@employeeEdit');
    Route::post('/update/{id}', 'UserController@employeeUpdate');
    Route::get('/delete/{id}', 'UserController@employeeDelete');
});

Route::prefix('/faq')->group(function () {
    Route::get('/', 'FaqsController@index');
    Route::get('/add', 'FaqsController@create');
    Route::post('/store', 'FaqsController@store');
    Route::get('/edit/{id}', 'FaqsController@edit');
    Route::post('/update/{id}', 'FaqsController@update');
    Route::get('/delete/{id}', 'FaqsController@delete');
});
