@extends('layouts.default')

@section('title', $page_title)

@section('css')
    <link href="/assets/plugins/jvectormap-next/jquery-jvectormap.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
@stop

@section('content')
    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <h1 class="page-header">Dashboard <small>header small text goes here...</small></h1>
        <!-- END page-header -->

        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-3 -->
            <div class="col-xl-3 col-md-6">
                <div class="widget widget-stats bg-blue">
                    <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                    <div class="stats-info text-white">
                        <h4>TOTAL USERS</h4>
                        <p>{{ $total_users }}</p>
                    </div>
                    <div class="stats-link">
                        <a href="/users">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- END col-3 -->
            <!-- BEGIN col-3 -->
            <div class="col-xl-3 col-md-6">
                <div class="widget widget-stats bg-info">
                    <div class="stats-icon"><i class="fa fa-link"></i></div>
                    <div class="stats-info text-white">
                        <h4>TOTAL ORDERS</h4>
                        <p>{{ $total_orders }}</p>
                    </div>
                    <div class="stats-link">
                        <a href="/home">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- END col-3 -->
            <!-- BEGIN col-3 -->
            <div class="col-xl-3 col-md-6">
                <div class="widget widget-stats bg-orange">
                    <div class="stats-icon"><i class="fa fa-users"></i></div>
                    <div class="stats-info text-white">
                        <h4>TOTAL EMPLOYEES</h4>
                        <p>{{ $total_employees }}</p>
                    </div>
                    <div class="stats-link">
                        <a href="/employee">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- END col-3 -->
            <!-- BEGIN col-3 -->
            <div class="col-xl-3 col-md-6">
                <div class="widget widget-stats bg-red">
                    <div class="stats-icon"><i class="fa fa-clock"></i></div>
                    <div class="stats-info text-white">
                        <h4>Total SALE</h4>
                        <p>{{ $total_sale }}</p>
                    </div>
                    <div class="stats-link">
                        <a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- END col-3 -->
        </div>
        <!-- END row -->

        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-8 -->
            <div class="col-xl-8">

                <!-- BEGIN tabs -->
                <ul class="nav nav-tabs nav-tabs-inverse nav-justified" data-sortable-id="index-2">
                    <li class="nav-item"><a href="#new_users" data-bs-toggle="tab" class="nav-link active"><i
                                class="fa fa-users fa-lg me-5px"></i> <span class="d-none d-md-inline">New Users</span></a>
                    </li>
                    <li class="nav-item"><a href="#pending_orders" data-bs-toggle="tab" class="nav-link"><i
                                class="fa fa-clock fa-lg me-5px"></i> <span class="d-none d-md-inline">Pending Orders</span></a>
                    </li>
                    <li class="nav-item"><a href="#paid_orders" data-bs-toggle="tab" class="nav-link"><i
                                class="fa fa-credit-card fa-lg me-5px"></i> <span class="d-none d-md-inline">Paid Orders</span></a>
                    </li>
                </ul>
                <div class="tab-content bg-white rounded-bottom mb-20px" data-sortable-id="index-3">
                    <div class="tab-pane fade active show" id="new_users">
                        <div class="h-300px" data-scrollbar="true">
                            @if (count($_new_users) > 0)
                                <table class="table table-panel mb-0">
                                    <thead>
                                        <tr>
                                            <?php
                                            
                                            $_th = current($_new_users);
                                            if (!empty($_th)) {
                                                foreach ($_th as $k => $title) {
                                                    if ($k != 'id') {
                                                        echo '<th>' . ucfirst(str_replace('_', ' ', $k)) . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($_new_users as $key => $p)
                                            <tr>
                                                @foreach ($p as $key => $value)
                                                    <td>{{ $value }} </td>
                                                @endforeach
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            @else
                                <table class="table table-panel mb-0">
                                    <tbody>
                                        <tr>
                                            <td>No Data Found </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pending_orders">
                        <div class="h-300px" data-scrollbar="true">
                            @if (count($_pending_orders) > 0)
                                <table class="table table-panel mb-0">
                                    <thead>
                                        <tr>
                                            <?php
                                            
                                            $_th = current($_pending_orders);
                                            if (!empty($_th)) {
                                                foreach ($_th as $k => $title) {
                                                    if ($k != 'id') {
                                                        echo '<th>' . ucfirst(str_replace('_', ' ', $k)) . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($_pending_orders as $key => $p)
                                            <tr>
                                                @foreach ($p as $key => $value)
                                                    <td>{{ $value }} </td>
                                                @endforeach
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            @else
                                <table class="table table-panel mb-0">
                                    <tbody>
                                        <tr>
                                            <td>No Data Found </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane fade" id="paid_orders">
                        <div class="h-300px" data-scrollbar="true">
                            @if (count($_paid_orders) > 0)
                                <table class="table table-panel mb-0">
                                    <thead>
                                        <tr>
                                            <?php
                                            
                                            $_th = current($_paid_orders);
                                            if (!empty($_th)) {
                                                foreach ($_th as $k => $title) {
                                                    if ($k != 'id') {
                                                        echo '<th>' . ucfirst(str_replace('_', ' ', $k)) . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($_paid_orders as $key => $p)
                                            <tr>
                                                @foreach ($p as $key => $value)
                                                    <td>{{ $value }} </td>
                                                @endforeach
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            @else
                                <table class="table table-panel mb-0">
                                    <tbody>
                                        <tr>
                                            <td>No Data Found </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- END tabs -->

            </div>
            <!-- END col-8 -->

        </div>
        <!-- END row -->
    </div>
@endsection

@section('script')
    <script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="/assets/plugins/flot/source/jquery.canvaswrapper.js"></script>
    <script src="/assets/plugins/flot/source/jquery.colorhelpers.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.saturated.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.browser.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.drawSeries.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.uiConstants.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.time.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.resize.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.pie.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.crosshair.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.categories.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.navigate.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.touchNavigate.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.hover.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.touch.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.selection.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.symbol.js"></script>
    <script src="/assets/plugins/flot/source/jquery.flot.legend.js"></script>
    <script src="/assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="/assets/plugins/jvectormap-next/jquery-jvectormap.min.js"></script>
    <script src="/assets/plugins/jvectormap-content/world-mill.js"></script>
    <script src="/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="/assets/js/demo/dashboard.js"></script>
@stop
