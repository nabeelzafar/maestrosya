@extends('layouts.default')

@section('title', $page_title)

@section('css')
    <link href="/assets/plugins/@fullcalendar/common/main.min.css" rel="stylesheet" />
    <link href="/assets/plugins/@fullcalendar/daygrid/main.min.css" rel="stylesheet" />
    <link href="/assets/plugins/@fullcalendar/timegrid/main.min.css" rel="stylesheet" />
    <link href="/assets/plugins/@fullcalendar/list/main.min.css" rel="stylesheet" />
    <link href="/assets/plugins/@fullcalendar/bootstrap/main.min.css" rel="stylesheet" />
@stop

@section('content')
    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <h1 class="page-header">Calendar <small>header small text goes here...</small></h1>
        <!-- END page-header -->
        <hr />
        <!-- BEGIN row -->
        <div class="row">
            <div class="col-lg">
                <!-- BEGIN calendar -->
                <div id="work_calendar" class="calendar"></div>
                <!-- END calendar -->
            </div>
        </div>
        <!-- END row -->
    </div>
@endsection


@section('script')
    <script src="/assets/plugins/moment/moment.js"></script>
    <script src="/assets/plugins/@fullcalendar/core/main.global.js"></script>
    <script src="/assets/plugins/@fullcalendar/daygrid/main.global.js"></script>
    <script src="/assets/plugins/@fullcalendar/timegrid/main.global.js"></script>
    <script src="/assets/plugins/@fullcalendar/interaction/main.global.js"></script>
    <script src="/assets/plugins/@fullcalendar/list/main.global.js"></script>
    <script src="/assets/plugins/@fullcalendar/bootstrap/main.global.js"></script>


    <script>
        $(document).ready(function() {
            console.log('{!! json_encode($_view_data) !!}');



            var calendarElm = document.getElementById('work_calendar');
            var calendar = new FullCalendar.Calendar(calendarElm, {
                initialView: 'dayGridMonth',
                editable: false,
                headerToolbar: {
                    left: false,
                    center: 'title',
                    right: false
                },
                themeSystem: 'bootstrap',
                views: {
                    timeGrid: {
                        eventLimit: 3
                    }
                },
                events: {!! json_encode($_view_data) !!}
            });

            calendar.render();
        });
    </script>
@stop
