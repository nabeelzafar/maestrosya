@extends('layouts.default')

@section('title', $page_title)

@section('content')
    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <h1 class="page-header">{{ $page_title }}</h1>
        <!-- END page-header -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 p-0">
                    <!-- BEGIN panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">{{ $page_title }}</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning"
                                    data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                                        class="fa fa-expand"></i></a>
                            </div>
                        </div>

                        @include('layouts.alerts_message')

                        <div class="panel-body p-0">
                            <form class="form-horizontal form-bordered" method="POST" action="{{ $_url }}">
                                @csrf
                                <input type="hidden" id="user_type" name="user_type" value="employee">
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2">Name</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="name" name="name" value="{{ isset($_view_data['name']) ? $_view_data['name'] : '' }}"
                                            placeholder="Enter Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2">Email</label>
                                    <div class="col-lg-8">
                                        <input type="email" class="form-control" id="email" name="email" value="{{ isset($_view_data['email']) ? $_view_data['email'] : '' }}"
                                            placeholder="Enter Email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2">Mobile No</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="mobile_no" name="mobile_no"  value="{{ isset($_view_data['mobile_no']) ? $_view_data['mobile_no'] : '' }}"
                                            placeholder="Enter Mobile No">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2">Address</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" id="address" name="address" rows="2" data-parsley-minlength="20"
                                            data-parsley-maxlength="100" placeholder="Enter Address">{{ isset($_view_data['address']) ? $_view_data['address'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2"></label>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END panel -->
                </div>
                <div class="col-md-2">
                    {{-- <a href="javascript:;" class="btn btn-success w-100">Test</a> --}}
                </div>
            </div>
        </div>

    </div>
@endsection
