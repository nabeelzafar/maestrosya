@section('outside-content')
    <!-- #modal-message -->
    <div class="modal modal-message fade" id="delete-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal Message Header</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <p>Text in a modal</p>
                    <p>Do you want to turn on location services so GPS can use your location ?</p>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-white" data-bs-dismiss="modal">Close</a>
                    <a href="javascript:;" class="btn btn-primary">Save Changes</a>
                </div>
            </div>
        </div>
    </div>
@endsection
