@extends('layouts.default')

@section('title', $page_title)

@section('css')
    @include('includes.datatable-css')
    <link href="/assets/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
@stop
@section('content')
    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <h1 class="page-header">{{ $page_title }}</h1>
        <!-- END page-header -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 p-0">
                    <!-- BEGIN panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">{{ $page_title }}</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning"
                                    data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                                        class="fa fa-expand"></i></a>
                            </div>
                        </div>

                        <div class="panel-body p-0">
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-dismissible fade show">
                                    {{ $message }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"></span>
                                </div>
                            @endif

                            <form class="form-horizontal form-bordered" method="POST" action="/order/update">
                                @csrf
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Title</label>
                                    <div class="col-lg-8">
                                        <input type="text" value="{{ $_view_data['title'] }}" class="form-control"
                                            disabled />    
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Service name</label>
                                    <div class="col-lg-8">
                                        <input type="text" value="{{ $_view_data['service']['name'] }}"
                                            class="form-control" disabled />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Customer name</label>
                                    <div class="col-lg-8">
                                        <input type="text" value="{{ $_view_data['user']['name'] }}"
                                            class="form-control" disabled />
                                        <a href="whatsapp://send?text={{ $_view_data['user']['mobile_no'] }}" title="Chat On Whatsapp">WhatsApp</a>    
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Description</label>
                                    <div class="col-lg-8">
                                        <div class="form-control p-0  overflow-hidden">
                                            <textarea class="textarea form-control" rows="4" disabled>{{ $_view_data['description'] }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Payment Method</label>
                                    <div class="col-lg-8">
                                        <input type="text" value="{{ $_view_data['payment_method'] }}"
                                            placeholder="Enter Notes ..." class="form-control" disabled />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Employee</label>
                                    <div class="col-lg-8">
                                        <select class="form-select" name="employee_id"  id="employee_id" required>
                                            <option value="">Please Select Employee</option>
                                            @foreach ($_employee as $employee)
                                                <option {!! !empty($employee['id']) && $employee['id'] === $_view_data['employee_id'] ? 'selected' : '' !!} value="{{ $employee['id'] }}">
                                                    {{ $employee['id'] . ' - ' . $employee['name'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Schedule At</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="schedule_at" id="schedule_at" value="{{ $_view_data['schedule_at'] }}"
                                            placeholder="Select Date Time ..." class="form-control" required />
                                    </div>
                                </div>

                                <input type="hidden" name="order_id" value="{{ $_view_data['id'] }}" />
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Notes</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="notes" value="{{ $_view_data['notes'] }}"
                                            placeholder="Enter Notes ..." class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Cost</label>
                                    <div class="col-lg-8">
                                        <input type="number" name="cost" value="{{ $_view_data['cost'] }}"
                                            placeholder="Enter Cost ..." class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-label col-form-label col-lg-4">Status</label>
                                    <div class="col-lg-8">
                                        <select class="form-select" name="status" id="status" required>
                                            <option {!! !empty($_view_data['status']) && $_view_data['status'] === 'pending' ? 'selected' : '' !!} value="" disabled>Pending</option>
                                            <option {!! !empty($_view_data['status']) && $_view_data['status'] === 'awaiting_payment' ? 'selected' : '' !!} value="awaiting_payment">Awaiting Payment
                                            </option>
                                            <option {!! !empty($_view_data['status']) && $_view_data['status'] === 'in progress' ? 'selected' : '' !!} value="in progress">In Progress</option>
                                            <option {!! !empty($_view_data['status']) && $_view_data['status'] === 'completed' ? 'selected' : '' !!} value="completed">Completed</option>
                                            <option {!! !empty($_view_data['status']) && $_view_data['status'] === 'canceled' ? 'selected' : '' !!} value="canceled">Canceled</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-4 col-form-label"></div>
                                    <div class="col-lg-8">
                                        <button class="btn btn-success" type="submit">Save Changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END panel -->
                </div>
                <div class="col-md-2">


                    {{-- <a href="javascript:;" class="btn btn-success w-100">Test</a> --}}
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    @include('includes.datatable-js')
    <script src="/assets/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#schedule_at').datetimepicker();
        });
    </script>
@stop
