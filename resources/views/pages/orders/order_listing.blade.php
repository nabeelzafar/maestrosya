@extends('layouts.default')

@section('title', $page_title)

@section('css')
    @include('includes.datatable-css')
@stop
@section('content')
    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <h1 class="page-header">{{ $page_title }}</h1>
        <!-- END page-header -->

        <div class="container-fluid">
             <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-3 -->
            <div class="col-xl-4 col-md-6">
                <div class="widget widget-stats bg-blue">
                    <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                    <div class="stats-info text-white">
                        <h4>TODAY SALE</h4>
                        <p>{{ $today_sale }}</p>
                    </div>
                </div>
            </div>
            <!-- END col-3 -->
            <!-- BEGIN col-3 -->
            <div class="col-xl-4 col-md-6">
                <div class="widget widget-stats bg-info">
                    <div class="stats-icon"><i class="fa fa-link"></i></div>
                    <div class="stats-info text-white">
                        <h4>TOTAL WEEKLY SALE</h4>
                        <p>{{ $weekly_sale }}</p>
                    </div>
                </div>
            </div>
            <!-- END col-3 -->
            <!-- BEGIN col-3 -->
            <div class="col-xl-4 col-md-6">
                <div class="widget widget-stats bg-orange">
                    <div class="stats-icon"><i class="fa fa-users"></i></div>
                    <div class="stats-info text-white">
                        <h4>TOTAL SALE</h4>
                        <p>{{ $total_sale }}</p>
                    </div>
                </div>
            </div>
            <!-- END col-3 -->
            
        </div>
        <!-- END row -->
            <div class="row">
                <div class="col-md-12 p-0">
                    <!-- BEGIN panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">{{ $page_title }}</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning"
                                    data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                                        class="fa fa-expand"></i></a>
                            </div>
                        </div>

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show">
                                {{ $message }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert"></span>
                            </div>
                        @endif
                        <div class="panel-body">
                            @if (count($_view_data) > 0)
                                <table class="table table-striped table-bordered data-table-combine">
                                    <thead>
                                        <tr>
                                            <?php
                                            
                                            $_th = current($_view_data);
                                            if (!empty($_th)) {
                                                foreach ($_th as $k => $title) {
                                                    if ($k != 'id') {
                                                        echo '<th>' . ucfirst(str_replace('_', ' ', $k)) . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($_view_data as $key => $p)
                                            <tr>
                                                <td>{{ $p['title'] }} </td>
                                                <td>{{ $p['service_name'] }}</td>
                                                <td>
                                                    {{ $p['customer']['name'] }}
                                                    <a href="whatsapp://send?text={{ $p['customer']['mobile_no'] }}" title="Chat On Whatsapp">WhatsApp</a>
                                                </td>
                                                <td>{{ $p['description'] }}</td>
                                                <td>{{ $p['notes'] }}</td>
                                                <td>{{ $p['cost'] }}</td>
                                                <td>{{ $p['payment_method'] }}</td>
                                                <td>{{ $p['status'] }}</td>
                                                <td>{!! $p['action'] !!}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            @else
                                <table class="table table-bordered ">
                                    <tbody>
                                        <tr>
                                            <td>No Data Found </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- END panel -->
                </div>
                <div class="col-md-2">


                    {{-- <a href="javascript:;" class="btn btn-success w-100">Test</a> --}}
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    @include('includes.datatable-js')
@stop
