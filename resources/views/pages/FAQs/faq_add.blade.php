@extends('layouts.default')

@section('title', $page_title)

@section('content')
    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <h1 class="page-header">{{ $page_title }}</h1>
        <!-- END page-header -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 p-0">
                    <!-- BEGIN panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">{{ $page_title }}</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning"
                                    data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                                        class="fa fa-expand"></i></a>
                            </div>
                        </div>

                        @include('layouts.alerts_message')

                        <div class="panel-body p-0">
                            <form class="form-horizontal form-bordered" method="POST" action="{{ $_url }}">
                                @csrf
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2">Title</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="title" name="title" value="{{ isset($_view_data['title']) ? $_view_data['title'] : '' }}"
                                            placeholder="Enter Title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2">Status</label>
                                    <div class="col-lg-8">
                                        <select class="form-select" id="status" name="status">
                                            <option {!! !empty($_view_data['status']) && $_view_data['status'] === "active" ? 'selected' : '' !!} value="active">Active</option>
                                            <option {!! !empty($_view_data['status']) && $_view_data['status'] === "inactive" ? 'selected' : '' !!} value="inactive">In Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2">Description</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" id="description" name="description" rows="2" data-parsley-minlength="20"
                                            data-parsley-maxlength="100" placeholder="Enter Description">{{ isset($_view_data['description']) ? $_view_data['description'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-label col-form-label col-md-2"></label>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END panel -->
                </div>
                <div class="col-md-2">
                    {{-- <a href="javascript:;" class="btn btn-success w-100">Test</a> --}}
                </div>
            </div>
        </div>

    </div>
@endsection
