@extends('layouts.default')

@section('title', $page_title)

@section('css')
    @include('includes.datatable-css')
@stop

@section('content')
    <div id="content" class="app-content">
        <!-- BEGIN breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
            <li class="breadcrumb-item active">{{ $page_title }}</li>
        </ol>
        <!-- END breadcrumb -->
        <!-- BEGIN page-header -->
        <h1 class="page-header">{{ $page_title }}</h1>
        <!-- END page-header -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 p-0">
                    <!-- BEGIN panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <h4 class="panel-title">{{ $page_title }}</h4>
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-warning"
                                    data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                                        class="fa fa-expand"></i></a>
                            </div>
                        </div>

                        @include('layouts.alerts_message')

                        <div class="panel-body">
                            @if (count($_view_data) > 0)
                                <table class="table table-striped table-bordered data-table-combine">
                                    <thead>
                                        <tr>
                                            <?php
                                            
                                            $_th = current($_view_data);
                                            if (!empty($_th)) {
                                                foreach ($_th as $k => $title) {
                                                    if ($k != 'id') {
                                                        echo '<th>' . ucfirst(str_replace('_', ' ', $k)) . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($_view_data as $key => $p)
                                            <tr>
                                                <td>{{ $p['title'] }} </td>
                                                <td>{{ $p['description'] }}</td>
                                                <td>{{ $p['status'] }}</td>
                                                <td>{!! $p['action'] !!}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            @else
                                <table class="table table-bordered ">
                                    <tbody>
                                        <tr>
                                            <td>No Data Found </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                    <!-- END panel -->
                </div>
                <div class="col-md-2">
                    <a href="/faq/add" class="btn btn-primary w-100"> <i class="fa fa-circle-plus"></i>Add FAQ</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @include('includes.datatable-js')
@stop
