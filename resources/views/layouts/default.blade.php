<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @include('includes.head')
</head>

<body class="pace-done">
    <div class="app-cover"></div>

    <!-- BEGIN #loader -->
    <div id="loader" class="app-loader">
        <span class="spinner"></span>
    </div>
    <!-- END #loader -->

    <div id="app" class="app app-sidebar-fixed app-header-fixed">

        @include('includes.header')

        @include('includes.sidebar')

        @yield('content')

    </div>

    @yield('outside-content')

    @include('includes.page-js')
</body>

</html>
