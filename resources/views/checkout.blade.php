<!DOCTYPE html>
<html>
<head>
<title>CHECKOUT BUTTON</title>
<meta name = "viewport" content = "ancho = ancho del dispositivo, escala inicial = 1">
{{-- <script type="text/javascript" src="js/jquery-3.6.0.min.js"></script> --}}
<script
  src="https://code.jquery.com/jquery-3.7.0.min.js"
  integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g="
  crossorigin="anonymous"></script>
<script src='https://eu-test.oppwa.com/v1/paymentWidgets.js?checkoutId={{ $checkout_content }}'></script> 
<script type="text/javascript">
    var wpwlOptions= {
        style: "card",
        locale: "es",
        labels: {cvv: "CVV", cardHolder: "Nombre(Igual que en la tarjeta)"},
            onBeforeSubmitCard: function(){
                if($(".wpwl-control-cardHolder").val()===""){
                    $(".wpwl-control-cardHolder").addClass("wpwl-has-error");
                    //wpwl-hint wpwl-hint-cardNumberError
                    $(".wpwl-control-cardHolder").after("<div class='wpwl-hint wpwl-hint-cardNumberError'>Nombre del titular de la tarjeta no valido</div>");
                    $(".wpwl-button-pay").addClass("wpwl-button-error").attr("disabled", "disabled");
                    return false;
                }else
                    return true;
                }
    }
</script>
</head>
<body>
    <form action='https://maestrosya.ec:8080/sandboxStatus' class='paymentWidgets' data-brands='VISA MASTER AMEX DISCOVER'></form>
</body>
{{-- <script type="text/javascript" src="https://www.datafast.com.ec/js/dfAdditionalValidations1.js"> --}}
</html>
                                            