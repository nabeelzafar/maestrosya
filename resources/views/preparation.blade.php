<!DOCTYPE html>
<html>
<head>
<title>CHECKOUT BUTTON</title>
{{-- <script type="text/javascript" src="js/jquery-3.6.0.min.js"></script> --}}
<script type=”text/javascript” src="js/jquery-3.2.1.js" > </script>
<script type="text/javascript" src="https://www.datafast.com.ec/js/dfAdditionalValidations1.js">
<script type="text/javascript">
    var wpwlOptions = {
        onReady: function(onReady) {
            var createRegistrationHtml = '<div class="customLabel">Desea guardar de manera segura sus datos?</div><div class="customInput"><input type="checkbox" name="createRegistration" /></div>';
            $('form.wpwl-form-card').find('.wpwl-button').before(createRegistrationHtml);
        },
        style: "card",
        locale: "es",
        labels: {cvv: "Codigo de verificación", cardholder: "Nombre(Igual que en la tarjeta),"},
        registrations: {
            requireCvv: true,
            hideInitialPaymentForms: true
        }
    }
</script>
</head>
<body>
    <form action='/sandboxStatus' class='wpwl-form-card' data-brands='VISA MASTER AMEX DISCOVER'></form>
</body>
</html>
                                            