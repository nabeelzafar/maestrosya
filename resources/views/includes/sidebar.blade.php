<!-- BEGIN #sidebar -->
<div id="sidebar" class="app-sidebar">
    <!-- BEGIN scrollbar -->
    <div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
        <div class="menu">
            <div class="menu-profile">
                <div class="menu-profile-cover with-shadow"></div>
                <div class="menu-profile-image">
                    <img src="/assets/img/user/user-13.jpg" alt="" />
                </div>
                <div class="menu-profile-info">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            {{ Auth::user()->name }}
                        </div>
                    </div>
                    <small>{{ Auth::user()->email }}</small>
                </div>
            </div>
            <div class="menu-divider m-0"></div>
            <div class="menu-header">Navigation</div>
            <div class="menu-item">
                <a href="/dashboard" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-sitemap"></i>
                    </div>
                    <div class="menu-text">Dashboard</div>
                </a>
            </div>
            <div class="menu-item">
                <a href="/calendar" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <div class="menu-text">Calendar</div>
                </a>
            </div>
            <div class="menu-item">
                <a href="/home" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="menu-text">Order</div>
                </a>
            </div>
            
            <div class="menu-item">
                <a href="/users" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="menu-text">User</div>
                </a>
            </div>
            <div class="menu-item">
                <a href="/employee" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-users-gear"></i>
                    </div>
                    <div class="menu-text">Employee</div>
                </a>
            </div>
            
            <div class="menu-item">
                <a href="/faq" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="menu-text">FAQ</div>
                </a>
            </div>
            <div class="menu-item">
                <a href="/services" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-wrench"></i>
                    </div>
                    <div class="menu-text">Service</div>
                </a>
            </div>
            <!-- BEGIN minify-button -->
            <div class="menu-item d-flex">
                <a href="javascript:;" class="app-sidebar-minify-btn ms-auto" data-toggle="app-sidebar-minify"><i
                        class="fa fa-angle-double-left"></i></a>
            </div>
            <!-- END minify-button -->

        </div>
        <!-- END menu -->
    </div>
    <!-- END scrollbar -->
</div>
<div class="app-sidebar-bg"></div>
<div class="app-sidebar-mobile-backdrop"><a href="#" data-dismiss="app-sidebar-mobile" class="stretched-link"></a>
</div>
<!-- END #sidebar -->
