<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <title>{{ str_replace('_', ' ', config('app.name')) }} | Login</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="/assets/css/vendor.min.css" rel="stylesheet" />
    <link href="/assets/css/app.min.css" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->
</head>

<body class="pace-done pace-top">
    <div class="app-cover"></div>

    <!-- BEGIN #loader -->
    <div id="loader" class="app-loader">
        <span class="spinner"></span>
    </div>
    <!-- END #loader -->

    <div id="app" class="app">

        <!-- BEGIN login -->
        <div class="login login-v1">
            <!-- BEGIN login-container -->
            <div class="login-container">
                <!-- BEGIN login-header -->
                <div class="login-header">
                    <div class="brand">
                        <div class="d-flex align-items-center">
                            <span class="logo"></span> <b>Color</b> Admin
                        </div>
                        <small>Bootstrap 5 Responsive Admin Template</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
                <!-- END login-header -->

                <!-- BEGIN login-body -->
                <div class="login-body">
                    <!-- BEGIN login-content -->
                    <div class="login-content fs-13px">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-floating mb-20px">
                                <input type="email" name="email" value="admin@admin.com"
                                    class="form-control fs-13px h-45px" id="emailAddress" placeholder="Email Address" />
                                <label for="emailAddress" class="d-flex align-items-center py-0">Email Address</label>
                            </div>
                            <div class="form-floating mb-20px">
                                <input type="password" name="password" value="admin@123"
                                    class="form-control fs-13px h-45px" id="password" placeholder="Password" />
                                <label for="password" class="d-flex align-items-center py-0">Password</label>
                            </div>
                            {{-- <div class="form-check mb-20px">
                                <input class="form-check-input" type="checkbox" value="" id="rememberMe" />
                                <label class="form-check-label" for="rememberMe">
                                    Remember Me
                                </label>
                            </div> --}}
                            <div class="login-buttons">
                                <button type="submit" class="btn h-45px btn-success d-block w-100 btn-lg">Sign
                                    in</button>
                            </div>
                        </form>
                    </div>
                    <!-- END login-content -->
                </div>
                <!-- END login-body -->
            </div>
            <!-- END login-container -->
        </div>
        <!-- END login -->

    </div>
    <!-- ================== BEGIN core-js ================== -->
    <script src="/assets/js/vendor.min.js"></script>
    <script src="/assets/js/app.min.js"></script>
    <!-- ================== END core-js ================== -->
</body>

</html>
